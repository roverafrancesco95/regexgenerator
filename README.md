# RegexGenerator

Si è deciso di modellare il problema nel seguente modo:

ogni regex ([Regex.java](src%2Fmain%2Fjava%2Forg%2Fexample%2Fmodels%2FRegex.java)) è composta da una lista di condizioni ([RegexCondition.java](src%2Fmain%2Fjava%2Forg%2Fexample%2Fmodels%2FRegexCondition.java)) che a loro volta contengono due proprietà:
* Tipologia dei caratteri (numeri o lettere)
* cardinalità (singola o range)

Ad esempio la seguente regex:
> [A-Z]{2}\d{3}[A-Z]{2,5}

sarà modellatra come di seguito:
```json
{
  "conditions": [
    {
      "type": "UPPER_LETTERS",
      "count": 2
    },
    {
      "type": "NUMBERS",
      "count": 3
    },
    {
      "type": "UPPER_LETTERS",
      "min": 2,
      "max": 5
    }
  ]
}
```

Ricevuta in input la lista di stringhe si procede subito con la relativa validazione per verificare che contengano solamente lettere maiuscole e numeri. 
Le stringhe che non dovessero soddisfare tali condizioni verranno semplicemente scartate.

Per determinare la regex corrisponde a una singola stringa dell'array in input si procede andandola a scansionare carattere per carattere identificando le varie RegexCondition che la compongono. Quest'ultime combinate insieme formeranno la regex specifica per quella stringa (metodo getRegex() della classe Regex).
Ripetendo il processo per tutte le stringhe della lista in input si otterrà quindi un array di oggetti Regex. A questo punto ogni input troverà corrispondenza con almeno una delle regex all'interno di questa lista.

> NB: Ad ogni iterazione si andrà a calcolare la regex che matcha con tutti gli input già processati. 
> In questo modo nelle iterazioni successive si eviterà di effettuare operazioni superflue nel caso in cui una stringa abbia già corrispondenza con la regex calcolata.

La classe delegata dell'individuazione di tutte le singole condizioni della regex finale è [RegexBuilder.java](src%2Fmain%2Fjava%2Forg%2Fexample%2Futils%2FRegexBuilder.java). 
Partendo dalla lista di regex specifiche per ciascuna singola stringha in input procederà generalizando le condizioni in modo tale da ottenere un unico oggetto Regex da cui, tramite il solito metodo getRegex(), sarà possibile ottenere la regex risultante.

> Per una maggiore robustezza del codice, si è deciso di testare (e di conseguenza di gestire) i casi in cui vengano inserite due o più RegexCondition dello stesso tipo di seguito (anche se è un caso che non può mai verificarsi in questo specifico esercizio).
> Vedi casi di test denominati con 'testRegexSpecialCaseOk'