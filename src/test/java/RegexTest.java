import org.example.enums.CharsTypeEnum;
import org.example.exceptions.ValidationException;
import org.example.models.Regex;
import org.example.models.RegexCondition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RegexTest {

    private Regex r;

    @BeforeEach
    public void init() {
        r = new Regex();
    }

    @Test
    public void testRegexOk1() {
        assertEquals(0, r.getGroupCount());
        assertEquals(0, r.getConditions().size());
        assertEquals("", r.getRegex());
    }

    @Test
    public void testRegexOk2() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 5));
        assertEquals(1, r.getGroupCount());
        assertEquals(1, r.getConditions().size());
        assertEquals("\\d{5}", r.getRegex());
    }

    @Test
    public void testRegexOk3() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 5));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 4));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 1));
        assertEquals(3, r.getGroupCount());
        assertEquals(3, r.getConditions().size());
        assertEquals("\\d{5}[A-Z]{4}\\d", r.getRegex());
    }

    @Test
    public void testRegexOk4() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 4));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 1));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1, 6));
        assertEquals(3, r.getGroupCount());
        assertEquals(3, r.getConditions().size());
        assertEquals("[A-Z]{4}\\d[A-Z]{1,6}", r.getRegex());
    }

    @Test
    public void testRegexOk5() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 4));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 1));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1, 6));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 1, 8));
        assertEquals(4, r.getGroupCount());
        assertEquals("[A-Z]{4}\\d[A-Z]{1,6}\\d{1,8}", r.getRegex());
    }

    @Test
    public void testRegexOk6() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 4));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 5, 7));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1, 6));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 1, 1));
        assertEquals(4, r.getGroupCount());
        assertEquals("[A-Z]{4}\\d{5,7}[A-Z]{1,6}\\d", r.getRegex());
    }

    @Test
    public void testRegexSpecialCaseOk1() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 2));
        assertEquals("[A-Z]{7}", r.getRegex());
    }

    @Test
    public void testRegexSpecialCaseOk2() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 2));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 2));
        assertEquals("\\d{4}", r.getRegex());
    }

    @Test
    public void testRegexSpecialCaseOk3() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1, 2));
        assertEquals("[A-Z]{6,7}", r.getRegex());
    }

    @Test
    public void testRegexSpecialCaseOk4() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5, 6));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 3));
        assertEquals("[A-Z]{8,9}", r.getRegex());
    }

    @Test
    public void testRegexSpecialCaseOk5() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5, 6));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 2, 3));
        assertEquals("[A-Z]{7,9}", r.getRegex());
    }

    @Test
    public void testRegexSpecialCaseOk6() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5, 6));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 2, 3));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 4));
        assertEquals("[A-Z]{11,13}", r.getRegex());
    }

    @Test
    public void testRegexSpecialCaseOk7() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5, 6));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 2, 3));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 4));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 0, 2));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 0, 2));
        assertEquals("[A-Z]{12,16}\\d{0,2}", r.getRegex());
    }

    @Test
    public void testRegexSpecialCaseOk8() throws ValidationException {
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5, 6));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 2, 3));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 4));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 0, 2));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 0, 2));
        r.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 0, 2));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 1, 2));
        r.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 2, 2));
        assertEquals("[A-Z]{12,16}\\d{0,2}[A-Z]\\d{3,6}", r.getRegex());
    }
}
