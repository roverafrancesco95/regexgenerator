import org.example.exceptions.ValidationException;
import org.example.services.RegexService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RegexServiceTest {
    private final RegexService service = new RegexService();

    @Test
    public void testConditionKo1() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> service.getRegex(null),
                "Expected ValidationValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals("Input list is null"));
    }

    @Test
    public void testConditionOk0() throws ValidationException {
        List<String> list = new ArrayList<>();
        String regex = service.getRegex(list);
        assertEquals("", regex);
    }

    @Test
    public void testConditionOk1() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("AB123ZZ");
        list.add("BB742TG");
        list.add("CF678HG");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{2}\\d{3}[A-Z]{2}", regex);
    }

    @Test
    public void testConditionOk2() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("TNTTST80A01F205E");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{6}\\d{2}[A-Z]\\d{2}[A-Z]\\d{3}[A-Z]", regex);
    }

    @Test
    public void testConditionOk3() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("AA123");
        list.add("BA1234");
        list.add("AB12345");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{2}\\d{3,5}", regex);
    }

    @Test
    public void testConditionOk4() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("A123XY");
        list.add("BA1234ZT");
        list.add("AB12345B");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{1,2}\\d{3,5}[A-Z]{1,2}", regex);
    }

    @Test
    public void testConditionOk5() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("A123XY");
        list.add("BAC1234ZT");
        list.add("AB12345B");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{1,3}\\d{3,5}[A-Z]{1,2}", regex);
    }

    @Test
    public void testConditionOk6() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("1A123456789000000000XY");
        list.add("BAC1234ZT");
        list.add("AB12345B");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{2,3}\\d{4,5}[A-Z]{1,2}", regex);
    }

    @Test
    public void testConditionOk7() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("AB123ZZ");
        list.add("BB742eeeeeTG");
        list.add("BB7ggggg42TG");
        list.add("BB?????742TG");
        list.add("CF678HG");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{2}\\d{3}[A-Z]{2}", regex);
    }

    @Test
    public void testConditionOk8() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("?AB123ZZ");
        list.add("aCF678HG");
        String regex = service.getRegex(list);
        assertEquals("", regex);
    }

    @Test
    public void testConditionOk9() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("aAB123ZZZ");
        list.add("CF678HG");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{2}\\d{3}[A-Z]{2}", regex);
    }

    @Test
    public void testConditionOk10() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("AB1234567890");
        list.add("CF678HG000AAAA0000");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{2}\\d{3,10}[A-Z]{0,2}\\d{0,3}[A-Z]{0,4}\\d{0,4}", regex);
    }

    @Test
    public void testConditionOk11() throws ValidationException {
        List<String> list = new ArrayList<>();
        list.add("AB1234567890");
        list.add("CF678HG000AAAA0000");
        String regex = service.getRegex(list);
        assertEquals("[A-Z]{2}\\d{3,10}[A-Z]{0,2}\\d{0,3}[A-Z]{0,4}\\d{0,4}", regex);

        list.add("A");
        regex = service.getRegex(list);
        assertEquals("[A-Z]{1,2}\\d{0,10}[A-Z]{0,2}\\d{0,3}[A-Z]{0,4}\\d{0,4}", regex);
    }

    @Test
    public void testConditionOk12() throws ValidationException {
        RegexService serviceMock = spy(RegexService.class);
        List<String> list = new ArrayList<>();
        list.add("AB1234z67890");
        list.add("CF678HG000AAAA0000");
        when(serviceMock.validateInput(list)).thenReturn(list);
        String regex = serviceMock.getRegex(list);
        assertEquals("[A-Z]{2}\\d{3}[A-Z]{2}\\d{3}[A-Z]{4}\\d{4}", regex);
    }

    @Test
    public void testConditionOk13() throws ValidationException {
        RegexService serviceMock = spy(RegexService.class);
        List<String> list = new ArrayList<>();
        list.add("AB1234#67890");
        list.add("CF678HG000AAAA0000");
        when(serviceMock.validateInput(list)).thenReturn(list);
        String regex = serviceMock.getRegex(list);
        assertEquals("[A-Z]{2}\\d{3}[A-Z]{2}\\d{3}[A-Z]{4}\\d{4}", regex);
    }
}
