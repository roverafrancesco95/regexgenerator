import org.example.enums.CharsTypeEnum;
import org.example.exceptions.ValidationException;
import org.example.models.RegexCondition;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RegexConditionTest {

    private final String TYPE_NULL_MSG = "CharsTypeEnum is null";
    private final String INVALID_CARDINALITY = "Invalid cardinality specified";
    private final String MIN_GREATER_THAN_MAX_NOT_VALID_MSG = "Min is greater than Max value";

    @Test
    public void testConditionKo1() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(null, 0),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(TYPE_NULL_MSG));
    }

    @Test
    public void testConditionKo2() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 0),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(INVALID_CARDINALITY));
    }

    @Test
    public void testConditionKo3() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(CharsTypeEnum.UPPER_LETTERS, -1),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(INVALID_CARDINALITY));
    }

    @Test
    public void testConditionKo4() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(null, 0, 0),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(TYPE_NULL_MSG));
    }

    @Test
    public void testConditionKo5() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 0, 0),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(INVALID_CARDINALITY));
    }

    @Test
    public void testConditionKo6() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 0, -1),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(INVALID_CARDINALITY));
    }

    @Test
    public void testConditionKo7() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(CharsTypeEnum.UPPER_LETTERS, -1, 5),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(INVALID_CARDINALITY));
    }

    @Test
    public void testConditionKo8() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(CharsTypeEnum.UPPER_LETTERS, -1, -1),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(INVALID_CARDINALITY));
    }

    @Test
    public void testConditionKo9() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 4, 2),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(MIN_GREATER_THAN_MAX_NOT_VALID_MSG));
    }

    @Test
    public void testConditionKo10() {
        ValidationException thrown = assertThrows(
                ValidationException.class,
                () -> new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 2, 1),
                "Expected ValidationException, but didn't"
        );

        assertTrue(thrown.getMessage().contentEquals(MIN_GREATER_THAN_MAX_NOT_VALID_MSG));
    }

    @Test
    public void testConditionOk1() throws ValidationException {
        int count = 1;
        RegexCondition rc = new RegexCondition(CharsTypeEnum.UPPER_LETTERS, count);
        assertEquals(count, rc.getMin());
        assertEquals(count, rc.getMax());
    }

    @Test
    public void testConditionOk2() throws ValidationException {
        int count = 5;
        RegexCondition rc = new RegexCondition(CharsTypeEnum.UPPER_LETTERS, count);
        assertEquals(count, rc.getMin());
        assertEquals(count, rc.getMax());
    }

    @Test
    public void testConditionOk3() throws ValidationException {
        int min = 1, max = 5;
        RegexCondition rc = new RegexCondition(CharsTypeEnum.UPPER_LETTERS, min, max);
        assertEquals(min, rc.getMin());
        assertEquals(max, rc.getMax());
    }

    @Test
    public void testConditionOk4() throws ValidationException {
        int min = 5, max = 5;
        RegexCondition rc = new RegexCondition(CharsTypeEnum.UPPER_LETTERS, min, max);
        assertEquals(min, rc.getMin());
        assertEquals(max, rc.getMax());
    }

    @Test
    public void testConditionOk5() throws ValidationException {
        RegexCondition rc = new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1);
        assertEquals("[A-Z]", rc.getRegexType());
    }

    @Test
    public void testConditionOk6() throws ValidationException {
        RegexCondition rc = new RegexCondition(CharsTypeEnum.DIGITS, 1);
        assertEquals("\\d", rc.getRegexType());
    }

    @Test
    public void testConditionOk7() throws ValidationException {
        RegexCondition rc = new RegexCondition(CharsTypeEnum.DIGITS, 1);
        assertEquals("", rc.getCharCount());
    }

    @Test
    public void testConditionOk8() throws ValidationException {
        RegexCondition rc = new RegexCondition(CharsTypeEnum.DIGITS, 2);
        assertEquals("{2}", rc.getCharCount());
    }

    @Test
    public void testConditionOk9() throws ValidationException {
        RegexCondition rc = new RegexCondition(CharsTypeEnum.DIGITS, 10);
        assertEquals("{10}", rc.getCharCount());
    }

    @Test
    public void testConditionOk10() throws ValidationException {
        RegexCondition rc = new RegexCondition(CharsTypeEnum.DIGITS, 1, 1);
        assertEquals("", rc.getCharCount());
    }

    @Test
    public void testConditionOk11() throws ValidationException {
        RegexCondition rc = new RegexCondition(CharsTypeEnum.DIGITS, 1, 2);
        assertEquals("{1,2}", rc.getCharCount());
    }

    @Test
    public void testConditionOk12() throws ValidationException {
        RegexCondition rc = new RegexCondition(CharsTypeEnum.DIGITS, 11, 23);
        assertEquals("{11,23}", rc.getCharCount());
    }

}
