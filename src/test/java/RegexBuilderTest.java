import org.example.enums.CharsTypeEnum;
import org.example.exceptions.ValidationException;
import org.example.models.Regex;
import org.example.models.RegexCondition;
import org.example.utils.RegexBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RegexBuilderTest {

    private RegexBuilder rb;

    @BeforeEach
    public void init() {
        rb = new RegexBuilder();
    }

    @Test
    public void testFinalRegexOk1() throws ValidationException {
        String regex = rb.getFinalRegex();
        assertEquals("", regex);
    }

    @Test
    public void testFinalRegexOk2() throws ValidationException {
        Regex regex = new Regex();
        regex.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5));
        rb.add(regex);
        rb.add(regex);
        String regexStr = rb.getFinalRegex();
        assertEquals("[A-Z]{5}", regexStr);
    }

    @Test
    public void testFinalRegexOk3() throws ValidationException {
        Regex regex = new Regex();
        regex.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 5));
        regex.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 0,2));
        regex.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 3));

        Regex regex2 = new Regex();
        regex2.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1));
        regex2.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1, 2));
        regex2.addCondition(new RegexCondition(CharsTypeEnum.DIGITS, 3));
        regex2.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1));

        Regex regex3 = new Regex();
        regex3.addCondition(new RegexCondition(CharsTypeEnum.UPPER_LETTERS, 1));

        rb.add(regex);
        assertEquals("[A-Z]{5}\\d{0,2}[A-Z]{3}", regex.getRegex());
        rb.add(regex2);
        assertEquals("[A-Z]{2,3}\\d{3}[A-Z]", regex2.getRegex());
        rb.add(regex3);
        assertEquals("[A-Z]", regex3.getRegex());

        String regexStr = rb.getFinalRegex();
        assertEquals("[A-Z]{1,5}\\d{0,3}[A-Z]{0,3}", regexStr);
    }
}
