package org.example.enums;

public enum CharsTypeEnum {
    DIGITS,
    UPPER_LETTERS,
    UNKNOWN_CHAR
}
