package org.example.models;

import org.example.enums.CharsTypeEnum;
import org.example.exceptions.ValidationException;

public class RegexCondition {
    private final CharsTypeEnum type;
    private int min;
    private int max;

    public RegexCondition(CharsTypeEnum type, int count) throws ValidationException {
        this(type, count, count);
    }

    public RegexCondition(CharsTypeEnum type, int min, int max) throws ValidationException {
        if (type == null) {
            throw new ValidationException("CharsTypeEnum is null");
        }
        this.type = type;

        if (min < 0 || max <= 0) {
            throw new ValidationException("Invalid cardinality specified");
        } else if (min == max) {
            this.min = min;
            this.max = max;
        } else if (min > max) {
            throw new ValidationException("Min is greater than Max value");
        } else {
            this.min = min;
            this.max = max;
        }
    }

    public String getRegexType() {
        return type == CharsTypeEnum.UPPER_LETTERS ? "[A-Z]" : "\\d";
    }

    public String getCharCount() {
        if (isRange()) {
            return String.format("{%d,%d}", min, max);
        } else {
            if (min == 1) {
                return "";
            } else {
                return String.format("{%d}", min);
            }
        }
    }

    public boolean isRange() {
        return max > min;
    }

    public CharsTypeEnum getType() {
        return type;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public void compressCondition(RegexCondition newCondition) {
        if (type != newCondition.getType()) {
            return;
        }

        min += newCondition.getMin();
        max += newCondition.getMax();
    }
}

