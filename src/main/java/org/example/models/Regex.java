package org.example.models;

import java.util.ArrayList;
import java.util.List;

public class Regex {

    private final List<RegexCondition> conditions;

    public Regex() {
        this.conditions = new ArrayList<>();
    }

    public String getRegex() {
        String regex = "";
        for (RegexCondition condition : conditions) {
            regex = String.format("%s%s%s", regex, condition.getRegexType(), condition.getCharCount());
        }
        return regex;
    }

    public void addCondition(RegexCondition condition) {
        if (conditions.isEmpty()) {
            conditions.add(condition);
            return;
        }

        RegexCondition lastCondition = conditions.get(conditions.size() - 1);
        if (lastCondition.getType() == condition.getType()) {
            lastCondition.compressCondition(condition);
        } else {
            conditions.add(condition);
        }
    }

    public int getGroupCount() {
        return conditions.size();
    }

    public List<RegexCondition> getConditions() {
        return conditions;
    }
}
