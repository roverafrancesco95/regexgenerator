package org.example.services;


import org.apache.commons.lang3.StringUtils;
import org.example.enums.CharsTypeEnum;
import org.example.exceptions.ValidationException;
import org.example.models.Regex;
import org.example.models.RegexCondition;
import org.example.utils.RegexBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.example.enums.CharsTypeEnum.*;

public class RegexService {
    private static final Logger logger = LoggerFactory.getLogger(RegexService.class);

    public String getRegex(List<String> input) throws ValidationException {
        List<String> validInput = validateInput(input);
        RegexBuilder rb = new RegexBuilder();

        String tmpRegex = "";
        for (String string : validInput) {
            // If the current regex already matches the current string skip the process (less iterations with big numbers)
            Pattern pattern = Pattern.compile(tmpRegex);
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                continue;
            }

            CharsTypeEnum currentCharType = null;
            int lettersCount = 0, digitsCount = 0;
            Regex newRegex = new Regex();
            try {
                for (int i = 0; i < string.length(); i++) {
                    char c = string.charAt(i);
                    currentCharType = getCurrentCharType(c);

                    switch (currentCharType) {
                        case DIGITS: {
                            if (lettersCount > 0) {
                                newRegex.addCondition(new RegexCondition(UPPER_LETTERS, lettersCount));
                                lettersCount = 0;
                            }
                            digitsCount++;
                            break;
                        }
                        case UPPER_LETTERS: {
                            if (digitsCount > 0) {
                                newRegex.addCondition(new RegexCondition(DIGITS, digitsCount));
                                digitsCount = 0;
                            }
                            lettersCount++;
                            break;
                        }
                        default:
                            throw new ValidationException(String.format("String [%s] contains unexpected char %c. Skip string", string, c));
                    }
                }
            } catch (ValidationException e) {
                logger.error("Error analyzing string, skip it.", e);
                continue;
            }
            int lastGroupCount = Math.max(lettersCount, digitsCount);
            newRegex.addCondition(new RegexCondition(currentCharType, lastGroupCount));

            rb.add(newRegex);
            tmpRegex = rb.getFinalRegex();
        }
        return tmpRegex;
    }

    private CharsTypeEnum getCurrentCharType(char c) {
        if (Character.isUpperCase(c)) {
            return UPPER_LETTERS;
        } else if (Character.isDigit(c)) {
            return DIGITS;
        }
        return UNKNOWN_CHAR;
    }

    public List<String> validateInput(List<String> input) throws ValidationException {
        if (input == null) {
            throw new ValidationException("Input list is null");
        }

        List<String> validString = new ArrayList<>();
        for (String string : input) {
            try {
                validateString(string);
                validString.add(string);
            } catch (Exception ignore) {
            }
        }
        return validString;
    }

    private void validateString(String input) throws ValidationException {
        if (StringUtils.isEmpty(input)) {
            throw new ValidationException("Input string is null or empty");
        }

        String ALPHA_NUMERIC_REGEX = "^[A-Z0-9]+$";
        if (!input.matches(ALPHA_NUMERIC_REGEX)) {
            throw new ValidationException("Input string contains unexpected chars");
        }

        if (!Character.isUpperCase(input.charAt(0))) {
            throw new ValidationException("Input string must start with a letter");
        }
    }
}
