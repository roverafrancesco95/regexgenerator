package org.example.utils;

import org.example.enums.CharsTypeEnum;
import org.example.exceptions.ValidationException;
import org.example.models.Regex;
import org.example.models.RegexCondition;

import java.util.ArrayList;
import java.util.List;

public class RegexBuilder {

    private final List<Regex> listOfRegex;

    public RegexBuilder() {
        listOfRegex = new ArrayList<>();
    }

    public void add(Regex newRegex) {
        listOfRegex.add(newRegex);
    }

    public String getFinalRegex() throws ValidationException {
        if (listOfRegex.size() == 0) {
            return "";
        } else if (listOfRegex.size() == 1) {
            return listOfRegex.get(0).getRegex();
        }

        int groupCount = getMaxGroupCount();
        Regex regex = new Regex();
        for (int i = 0; i < groupCount; i++) {
            RegexCondition condition = getConditionsByGroupIndex(i);
            regex.addCondition(condition);
        }
        return regex.getRegex();
    }

    private RegexCondition getConditionsByGroupIndex(int group) throws ValidationException {
        int min = Integer.MAX_VALUE, max = 0;

        CharsTypeEnum type = null;
        List<RegexCondition> originalConditions = new ArrayList<>();
        for (Regex regex : listOfRegex) {
            if (regex.getConditions().size() > group) {
                originalConditions.add(regex.getConditions().get(group));
                if (type == null) {
                    type = regex.getConditions().get(group).getType();
                }
            } else {
                min = 0;
            }
        }

        for (RegexCondition condition : originalConditions) {
            min = Math.min(min, condition.getMin());
            max = Math.max(max, condition.getMax());
        }
        return new RegexCondition(type, min, max);
    }

    private int getMaxGroupCount() {
        int count = 0;
        for (Regex regex : listOfRegex) {
            count = Math.max(count, regex.getGroupCount());
        }
        return count;
    }
}
