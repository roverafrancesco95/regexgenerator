package org.example;

import org.example.exceptions.ValidationException;
import org.example.services.RegexService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws ValidationException {
        RegexService service = new RegexService();

        List<String> list = new ArrayList<>();
        list.add("AB123ZZ");
        list.add("BB742TG");
        list.add("CF678HG");
        System.out.println(service.getRegex(list));
        list.clear();

        list.add("TNTTST80A01F205E");
        list.add("RVRFNC95A01F205E");
        System.out.println(service.getRegex(list));
        list.clear();

        list.add("AA123");
        list.add("BA1234");
        list.add("AB12345");
        System.out.println(service.getRegex(list));
        list.clear();

        list.add("A123XY");
        list.add("BA1234ZT");
        list.add("AB12345B");
        System.out.println(service.getRegex(list));
    }
}